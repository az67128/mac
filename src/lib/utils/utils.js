export function pickUnique(arr, qnty=3) {
	let result = [];
	let copyArr = [...arr];

	for (let i = 0; i < qnty; i++) {
		let randomIndex = Math.floor(Math.random() * copyArr.length);
		result.push({ ...copyArr[randomIndex]});
		copyArr.splice(randomIndex, 1);
	}
	return result;
}
