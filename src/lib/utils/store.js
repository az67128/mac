// store.js
import { writable } from 'svelte/store';

// Функция для инициализации стора данными из localStorage или пустым массивом, если данных нет
function createNotesStore() {
	// Попытаемся получить данные из localStorage
	const data = typeof window !== 'undefined' ? window?.localStorage.getItem('notes') : null;
	const initialNotes = data ? JSON.parse(data) : [];

	// Создаем стор с начальными данными
	const { subscribe, set, update } = writable(initialNotes);

	// Возвращаем объект стора с методами subscribe, set, update, и добавим метод для добавления новой заметки
	return {
		subscribe,
		set,
		addNote: (note) => {
			update((notes) => {
				const newNotes = [note, ...notes];
				localStorage.setItem('notes', JSON.stringify(newNotes));
				return newNotes;
			});
		},
		removeNote: (id) => {
			update((notes) => {
				const newNotes = notes.filter((item) => item.id !== id);
				localStorage.setItem('notes', JSON.stringify(newNotes));
				return newNotes;
			});
		},
		updateNote: (updatedNote) => {
			update((notes) => {
				const noteIndex = notes.findIndex((note) => note.id === updatedNote.id);
				let newNotes;

				if (noteIndex !== -1) {
					// Заменяем существующую запись новыми данными
					newNotes = notes.map((note, index) =>
						index === noteIndex ? { ...note, note: updatedNote.note } : note
					);
				} else {
					// Добавляем новую запись, если не нашли по ID

					newNotes = [...notes, { ...updatedNote, created: new Date().toISOString() }];
				}

				localStorage.setItem('notes', JSON.stringify(newNotes));
				return newNotes;
			});
		}
	};
}

// Экспортируем инстанс стора, чтобы его можно было использовать в компонентах
export const notesStore = createNotesStore();
